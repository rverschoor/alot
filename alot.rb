#!/usr/bin/env ruby
# frozen_string_literal: true

require 'faraday'
require 'faraday/retry'
require 'json'

# Zendesk API service
class Zendesk
  def initialize
    @data = nil
    @paginate_delay = 10
    @log_rate_limits = false
    @log_pagination_info = true
    @conn = Faraday.new(ENV.fetch('ZD_URL')) do |config|
      config.request :retry, retry_options
      config.response :json
      config.request :authorization, :basic, "#{ENV.fetch('ZD_USERNAME')}/token", ENV.fetch('ZD_TOKEN')
      # Log request
      # config.response :logger, nil, { headers: false, bodies: false }
    end
  end

  def get(endpoint, params = nil)
    resp = @conn.get endpoint, params
    show_rate_limits resp if @log_rate_limits
    handle_status resp
    handle_body resp
  end

  def get_paginate(endpoint, params = nil)
    url = endpoint
    loop do
      @data = get url, params
      show_pagination_info if @log_pagination_info
      yield @data
      url = next_page
      sleep @paginate_delay if more?
      break unless more?
    end
  end

  def more?
    @data['meta']['has_more']
  end

  def next_page
    @data['links']['next']
  end

  private

  def retry_options
    { retry_statuses: [429] } # Retry when hitting rate limit
  end

  def handle_body(resp)
    @data = resp.body
  end

  def handle_status(resp)
    return unless resp.status != 200

    puts "FAIL: Status #{resp.status}"
    exit 2
  end

  def show_pagination_info
    puts "has_more: #{more?}"
    puts "next_page: #{next_page}"
  end

  def show_rate_limits(resp)
    header = resp.env.response_headers
    print "x-rate-limit: #{header['x-rate-limit-remaining']}/#{header['x-rate-limit']}  "
    print "ratelimit-limit: #{header['ratelimit-remaining']}/#{header['ratelimit-limit']}  "
    puts "ratelimit-reset: #{header['ratelimit-reset']}"
  end
end

# Zendesk search class
class Search
  def initialize
    @zd = Zendesk.new
    initialize_retention_tickets
  end

  def retention_tickets(&block)
    @zd.get_paginate @search_tickets_url, &block
  end

  private

  def initialize_retention_tickets
    page_size = 100 # Can be max 1000 tickets/page
    retention_tag = '30d_retention'
    retention_date = '>30days'
    query = "status:closed has_attachment:true -tags:#{retention_tag} updated#{retention_date}"
    @search_tickets_url = "search/export?filter[type]=ticket&page[size]=#{page_size}&query=#{query}"
  end
end

# Application class
class Alot
  def initialize
    @search = Search.new
    @nr_tickets = 0
  end

  def process
    @search.retention_tickets do |data|
      process_tickets data
      puts
    end
  end

  private

  def process_tickets(data)
    tickets = data['results']
    @nr_tickets += tickets.count
    puts "Found #{tickets.count} tickets (total #{@nr_tickets})"
  end
end

# alot = Alot.new
# alot.process

# Testing ticket attachment info retrieval
# class Test
#   def initialize
#     @zd = Zendesk.new
#   end
#
#   def run
#     data = @zd.get 'tickets/512813/metrics', {}
#     # jj data
#     puts "status_updated_at: #{data['ticket_metric']['status_updated_at']}"
#   end
# end

require 'date'
# Testing: get nr of attachments
class Test
  def initialize
    @zd = Zendesk.new
  end

  def tickets_per_month
    url = 'https://gitlab.zendesk.com/api/v2/search?'
    %w[created updated].each do |filter|
      puts "Counting '#{filter}' tickets"
      (2023..2023).each do |year|
        total = 0
        (1..12).each do |month|
          start_date = Date.new(year, month, 1)
          end_date = Date.new(year, month, -1)
          query = "query=type:ticket status:closed has_attachment:true #{filter}>=#{start_date} #{filter}<=#{end_date}"
          data = @zd.get url + query, {}
          nr = data['count']
          total += nr
          puts "#{year}-#{month}: #{nr}"
          sleep 10
        end
        puts "Total: #{total}"
      end
    end
  end

  def tickets_per_year
    total = 0
    url = 'https://gitlab.zendesk.com/api/v2/search?'
    (2020..2024).each do |year|
      start_date = Date.new(year, 1, 1)
      end_date = Date.new(year, 12, 31)
      query = "query=type:ticket status:closed has_attachment:true updated>=#{start_date} updated<=#{end_date}"
      data = @zd.get url + query, {}
      nr = data['count']
      total += nr
      puts "#{year}: #{nr}"
      sleep 10
    end
    puts "Total: #{total}"
  end
end

test = Test.new
test.tickets_per_month

